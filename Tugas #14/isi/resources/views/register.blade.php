@extends('template.master')
@section('judul')
Buat Account Baru!
@endsection
@section('judul2')
    Sign Up Form
    @endsection
@section('isi')
    <form action="/kirim" method="POST">
        @csrf
        <label for="first">First name:</label>
        <input type="text" id="first" name="fname">
        <label for="last">Last Name</label>
        <input type="text" id="last" name="lname"><br><br>
        <label>Gender:</label>
        <input type="radio" name="male">Male
        <input type="radio" name="female">Female
        <input type="radio" name="other">Other
        <br>
        <label>Nationality</label>
        <select name="country">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="australia">Australia</option>
        </select><br>
        <label for="bio">Bio:</label><br>
        <textarea cols="30" rows="10" name="bio"></textarea><br>
        <button type="submit" class="btn btn-light">Kirim</button>
    </form>
@endsection
