<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    public function tables(){
        return view('data-pages.tables');
    }
    public function datatable(){
        return view('data-pages.data-tables');
    }
}
