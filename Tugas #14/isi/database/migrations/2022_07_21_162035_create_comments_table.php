<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id_comment');
            $table->unsignedinteger('id_user');
            $table->foreign('id_user')
                ->references('id_user')->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_film');
            $table->foreign('id_film')
                ->references('id_film')->on('film')
                ->onDelete('cascade');
            $table->text('comment');
            $table->integer('point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
