@extends('template.master')
@section('judul')
    SanberBook
    @endsection
    @section('judul2')
    Social Media Developer Santai Berkualitas
    @endsection
    @section('isi')
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p><br>
    <h3>Benefit Join di SanberBook</h3>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar Di
            <a href="/register">Form Sign Up</a>
        </li>
        <li>Selesai!</li>
    </ol>
@endsection
