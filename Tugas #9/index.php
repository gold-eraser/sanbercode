<?php
require_once "animal.php";
require_once "Frog.php";
require_once "Ape.php";
$shaun = new animals("Shaun The Si'ip", "4", "No");
new_line();
echo $shaun->show_info();
new_line();
$frog = new frog("Buduk", "4", "No");
echo $frog->show_info();
echo "<br>";
echo "Jump : {$frog->jump()}";
new_line();
$ape = new ape("Kera Sakti", "2", "No");
echo $ape->show_info();
echo "<br>";
echo "Yell : {$ape->yell()}";