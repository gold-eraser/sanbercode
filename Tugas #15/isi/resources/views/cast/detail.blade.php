@extends('template.master')
@section('judul')
    Single Caster Pages
@endsection
@section('judul2')
    Detail Caster
@endsection
@section('isi')
    <section class="ml-3">
        <h1 class="text-primary">{{ $cast->nama_cast }}</h1>
        <h3>{{ $cast->umur_cast }}</h3>
        <h5>{{ $cast->bio_cast }}</h5>
    </section>
    <a href="/cast" class="btn btn-primary" style="float: right">Back</a>
@endsection
