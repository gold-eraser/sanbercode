@extends('template.master')
@section('judul')
    Pages Edit Caster
@endsection
@section('judul2')
    Edit Caster
@endsection
@section('isi')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Form Edit Caster</h3>
        </div>
        <form role="form" action="/cast/{{ $cast->id_cast }}" method="POST">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label for="namacaster">Nama Caster</label>
                    <input type="text" class="form-control" id="namacaster" placeholder="Enter Caster Name"
                        name="nama" value="{{ $cast->nama_cast }}">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="umurcaster">Umur Caster</label>
                    <input type="number" class="form-control" id="umurcaster" placeholder="Enter Caster Age"
                        name="umur" value="{{ $cast->umur_cast }}">
                </div>
                @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="biocaster">Bio Caster</label>
                    <input type="text" class="form-control" id="biocaster" placeholder="Enter Caster Bio" name="bio" value="{{ $cast->bio_cast }}">
                </div>
                @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Edit Caster</button>
            </div>
        </form>
    </div>
@endsection
