@extends('template.master')
@section('judul')
    Pages Add Caster
@endsection
@section('judul2')
    Add Caster
@endsection
@section('isi')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Form Tambah Caster</h3>
        </div>
        <form role="form" action="/cast" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="namacaster">Nama Caster</label>
                    <input type="text" class="form-control" id="namacaster" placeholder="Enter Caster Name"
                        name="nama" value="{{old('nama')}}">
                </div>
                @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="umurcaster">Umur Caster</label>
                    <input type="number" class="form-control" id="umurcaster" placeholder="Enter Caster Age"
                        name="umur" value="{{old('umur')}}">
                </div>
                @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="biocaster">Bio Caster</label>
                    <input type="text" class="form-control" id="biocaster" placeholder="Enter Caster Bio" name="bio" value="{{old('bio')}}">
                </div>
                @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Add Caster</button>
            </div>
        </form>
    </div>
@endsection
