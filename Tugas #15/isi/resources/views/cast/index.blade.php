@extends('template.master')
@section('judul')
    Pages List Caster
@endsection
@section('judul2')
    List Caster
@endsection
@section('isi')
<a href="/cast/create" class="btn btn-info mb-3">Tambah Caster</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key =>$value)
            <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $value->nama_cast }}</td>
                <td>{{ $value->umur_cast }}</td>
                <td>{{ $value->bio_cast }}</td>
                <td>
                    <form action="/cast/{{ $value->id_cast }}" method="POST" role="form">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{ $value->id_cast }}" class="btn btn-info">Detail</a>
                        <a href="/cast/{{ $value->id_cast }}/edit" class="btn btn-warning">Edit</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
                @empty
                <th>Tidak Ada Data</th>
                <td>Tidak Ada Data</td>
                <td>Tidak Ada Data</td>
                <td>Tidak Ada Data</td>
                <td>Tidak Ada Data</td>
            </tr>
                @endforelse
        </tbody>
    </table>
@endsection
