<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate(
            [
                'nama' => 'required|min:3',
                'umur' => 'required|max:10',
                'bio' => 'required|min:3',
            ],
            [
                'nama.required'    => 'Kolom Nama Tidak Boleh Kosong',
                'umur.required' => 'Kolom Umur Tidak Boleh Kosong',
                'bio.required' => 'Kolom Bio Tidak Boleh Kosong',
                'nama.min' => 'Kolom Nama Minimal Berisi 3 Karakter',
                'umur.max' => 'Kolom Umur Tidak Boleh Berisi Lebih Dari 10 Karakter',
                'bio.min' => 'Kolom Bio Harus Berisi Minimal 3 Karakter',
            ]
        );
        DB::table('cast')->insert(
            [
                'nama_cast' => $request['nama'],
                'umur_cast' => $request['umur'],
                'bio_cast' => $request['bio']
            ]
        );
        return redirect('/cast');
    }
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }
    public function show($id)
    {
        $cast = DB::table('cast')->where('id_cast', $id)->first();
        return view('cast.detail', compact('cast'));
    }
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id_cast', $id)->first();
        return view('cast.edit', compact('cast'));
    }
    public function update(Request $request,$id)
    {
        $request->validate(
            [
                'nama' => 'required|min:3',
                'umur' => 'required|max:10',
                'bio' => 'required|min:3',
            ],
            [
                'nama.required'    => 'Kolom Nama Tidak Boleh Kosong',
                'umur.required' => 'Kolom Umur Tidak Boleh Kosong',
                'bio.required' => 'Kolom Bio Tidak Boleh Kosong',
                'nama.min' => 'Kolom Nama Minimal Berisi 3 Karakter',
                'umur.max' => 'Kolom Umur Tidak Boleh Berisi Lebih Dari 10 Karakter',
                'bio.min' => 'Kolom Bio Harus Berisi Minimal 3 Karakter',
            ]
        );
        DB::table('cast')
        ->where('id_cast',$id)
        ->update([
            'nama_cast'=>$request['nama'],
            'umur_cast'=>$request['umur'],
            'bio_cast'=>$request['bio'],
        ]);
        return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id_cast',$id)->delete();
        return redirect('/cast');
    }
}
