<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function kirim(Request $req){
        $namadepan=$req['fname'];
        $namabelakang=$req['lname'];
        return view('welcome', compact('namadepan','namabelakang'));
    }
}
