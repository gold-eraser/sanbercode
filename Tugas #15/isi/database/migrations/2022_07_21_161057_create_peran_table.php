<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran', function (Blueprint $table) {
            $table->bigIncrements('id_peran');
            $table->unsignedBigInteger('id_film');
            $table->foreign('id_film')
            ->references('id_film')->on('film')
            ->onDelete('cascade');
            $table->unsignedBigInteger('id_cast');
            $table->foreign('id_cast')
            ->references('id_cast')->on('cast')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran');
    }
}
